FROM openjdk:8-jdk-alpine
RUN ./mvnw clean install -Dmaven.test.skip=true
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
